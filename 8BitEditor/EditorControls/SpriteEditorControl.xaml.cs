﻿namespace EightBitGameTools.EditorControls
{
    using EightBitGameTools;
    using EightBitGameTools.Convertors;
    using EightBitGameTools.Utility;
    using Microsoft.Win32;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.Json;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using Point = System.Drawing.Point;

    /// <summary>
    /// Interaction logic for SpriteEditorControl.xaml
    /// </summary>
    public partial class SpriteEditorControl : UserControl, IEditorControl
    {
        private byte activeColorIndex;
        private SpriteInfo currentSprite;
        private readonly ISpriteConvertor spriteConvertor;
        private int paletteSelectIndex = 0;
        private readonly Stack<string> UndoBuffer = new();
        private string lastPath = null;

        public SpriteEditorControl()
        {
            InitializeComponent();
            spriteConvertor = new SpriteConvertorCoco2();// SpriteConvertorBBC2Bit();

            // Configure BBC Sprite convertor for now
            ClearCurrentSprite();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ClearCurrentSprite();
        }

        #region Canvas handlers
        /// <summary>
        /// Rebuild the canvas grid array
        /// </summary>
        private void RebuildCanvas()
        {
            ResizeGridCanvas();
            SetToolbarPalette();

            GridSizeX.Text = currentSprite.Dimensions.X.ToString();
            GridSizeY.Text = currentSprite.Dimensions.Y.ToString();

            gridCanvas.Dimensions = currentSprite.Dimensions;
            gridCanvas.Clear(currentSprite.Palette.GetColor(0));

            for (int y = 0; y < currentSprite.Dimensions.Y; y++)
            {
                for (int x = 0; x < currentSprite.Dimensions.X; x++)
                {
                    byte val = currentSprite.GridArray[y][x];
                    Point point = new Point(x, y);
                    if (val > 0) SetCellColour(point, val, true);
                }
            }

            // Set preview image
            BuildSpritePreview();

            // Rebuild the code
            RebuildCode();
        }

        private void BuildSpritePreview()
        {
            SpritePreview.Source = BitmapConverter.BitmapToImageSource(currentSprite.UpdateBitmap());
        }

        private void SetCellColour(System.Drawing.Point point, byte index, bool deferBuild = false)
        {
            var colour = currentSprite.Palette.GetColor(index);
            gridCanvas.AddOrUpdateRect(point, colour);

            // Set colour in grid array
            currentSprite.GridArray[point.Y][point.X] = index;

            // Rebuild the code
            if (!deferBuild)
            {
                RebuildCode();
            }
        }

        private void gridCanvas_MouseDown(object sender, GridCanvasEventArgs data)
        {
            PushUndoBuffer();
            SetCellColour(data.Cell, activeColorIndex);

        }

        private void gridCanvas_MouseMove(object sender, GridCanvasEventArgs data)
        {
            if (((MouseEventArgs)data.EventArgs).LeftButton == MouseButtonState.Pressed)
            {
                SetCellColour(data.Cell, activeColorIndex);
            }
        }

        private void gridCanvas_GridMouseUp(object sender, GridCanvasEventArgs data)
        {
            BuildSpritePreview();
        }

        #endregion

        #region Toolbar handlers
        private void PaletteSelect_Click(object sender, RoutedEventArgs e)
        {
            // Clear active buttons
            int count = VisualTreeHelper.GetChildrenCount(ColourPalette);
            for (int i = 0; i < count; i++)

            {
                Button? ctrl = VisualTreeHelper.GetChild(ColourPalette, i) as Button;
                if (ctrl != null)
                {
                    ctrl.BorderBrush = new SolidColorBrush(Colors.White);
                    ctrl.BorderThickness = new Thickness(1);
                }
            }

            var button = (Button)sender;
            activeColorIndex = byte.Parse((string)button.Tag);
            button.BorderBrush = new SolidColorBrush(Colors.Black);
            button.BorderThickness = new Thickness(2);
        }

        private void PaletteSelect_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            var button = (Button)sender;
            paletteSelectIndex = byte.Parse((string)button.Tag);
            TestPopup.IsOpen = true;
        }

        private void PaletteSet_Click(object sender, RoutedEventArgs e)
        {
            PushUndoBuffer();
            var button = (Button)sender;
            var colour = (string)button.Tag;
            currentSprite.Palette[paletteSelectIndex] = colour;

            TestPopup.IsOpen = false;
            RebuildCanvas();
        }

        private void MenuLoad_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }

        private void MenuSaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveAs();
        }

        private void MenuSave_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void MenuClear_Click(object sender, RoutedEventArgs e)
        {
            ClearCurrentSprite();
        }

        private void GridSize_Click(object sender, RoutedEventArgs e)
        {
            var xSize = int.Parse(GridSizeX.Text);
            var ySize = int.Parse(GridSizeY.Text);
            currentSprite.SetGridSize(new Point(xSize, ySize));
            gridCanvas.Dimensions = currentSprite.Dimensions;
            RebuildCanvas();
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void SpriteName_TextChanged(object sender, TextChangedEventArgs e)
        {
            currentSprite.Name = SpriteName.Text;
            RebuildCode();
        }
        #endregion

        private void SetToolbarPalette()
        {
            foreach (Button button in ColourPalette.Children)
            {
                var colourIndex = byte.Parse((string)button.Tag);
                if (colourIndex >= currentSprite.Palette.Size)
                {
                    button.Visibility = Visibility.Hidden;
                }
                else
                {
                    button.Visibility = Visibility.Visible;
                    button.Background = new SolidColorBrush(currentSprite.Palette.GetColor(colourIndex));
                }
            }
        }

        private void ClearCurrentSprite()
        {
            PushUndoBuffer();
            currentSprite = new SpriteInfo(new Point(16, 16), spriteConvertor.GetDefaultPalette());
            SpriteName.Text = currentSprite.Name;

            SetToolbarPalette();

            gridCanvas.Dimensions = currentSprite.Dimensions;
            gridCanvas.Clear();

            GridSizeX.Text = currentSprite.Dimensions.X.ToString();
            GridSizeY.Text = currentSprite.Dimensions.Y.ToString();

            BuildSpritePreview();
            RebuildCode();
        }

        private void RebuildCode()
        {
            CodeOutput.Text = spriteConvertor.BuildCode(currentSprite);
        }

        #region Common Interface Actions
        public void ClearUndoBuffer()
        {
            UndoBuffer.Clear();
            StatusMessage.Text = UndoBuffer.Count.ToString();
        }

        public void PushUndoBuffer()
        {
            UndoBuffer.Push(JsonSerializer.Serialize(currentSprite));
            StatusMessage.Text = UndoBuffer.Count.ToString();
        }

        public void PopUndoBuffer()
        {
            if (UndoBuffer.Count == 0) return;
            var spritedata = UndoBuffer.Pop();
            currentSprite = JsonSerializer.Deserialize<SpriteInfo>(spritedata);
            StatusMessage.Text = UndoBuffer.Count.ToString();
            RebuildCanvas();
        }

        public void Save()
        {
            if (!currentSprite.Save())
            {
                SaveAs();
            }

            // Prevent clicking on the grid while double clicking the dialog...
            gridCanvas.LockCanvas();
        }

        public void Load()
        {
            ClearUndoBuffer();
            OpenFileDialog openFileDialog = new();
            openFileDialog.Filter = "Sprite file (*.8bs)|*.8bs|All files (*.*)|*.*";
            if (lastPath != null) openFileDialog.InitialDirectory = lastPath;
            if (!openFileDialog.ShowDialog() ?? false) return;
            lastPath = Path.GetDirectoryName(openFileDialog.FileName);

            gridCanvas.LockCanvas();

            currentSprite = SpriteInfo.Load(openFileDialog.FileName);
            if (currentSprite == null)
            {
                MessageBox.Show("Sprite load failed", "LOAD FAILED", MessageBoxButton.OK, MessageBoxImage.Error);
                currentSprite = new SpriteInfo(new Point(16, 16), spriteConvertor.GetDefaultPalette());
            }
            else
            {
                SpriteName.Text = currentSprite.Name;
            }

            RebuildCanvas();
        }

        public void SaveAs()
        {
            SaveFileDialog saveFileDialog = new();
            saveFileDialog.Filter = "Sprite file (*.8bs)|*.8bs";
            saveFileDialog.AddExtension = true;
            if (!saveFileDialog.ShowDialog() ?? false) return;
            currentSprite.Save(saveFileDialog.FileName);
            SpriteName.Text = currentSprite.Name;

            // Prevent clicking on the grid while double clicking the dialog...
            gridCanvas.LockCanvas();
        }
        #endregion

        public void ResizeGridCanvas()
        {
            double ratio = (double)currentSprite.Dimensions.X / currentSprite.Dimensions.Y;
            CanvasContainerGrid.ColumnDefinitions[0].Width = new GridLength(CanvasContainerGrid.ActualHeight * ratio);
        }

        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ResizeGridCanvas();
        }

        private void FlipVertical_Click(object sender, RoutedEventArgs e)
        {
            currentSprite.FlipVertical();
            RebuildCanvas();
            BuildSpritePreview();
            RebuildCode();

        }

        private void FlipHorizontal_Click(object sender, RoutedEventArgs e)
        {
            currentSprite.FlipHorizontal();
            RebuildCanvas();
            BuildSpritePreview();
            RebuildCode();
        }

        private void ShiftLeft_Click(object sender, RoutedEventArgs e)
        {
            PushUndoBuffer();
            currentSprite.ShiftLeft();
            RebuildCanvas();
            BuildSpritePreview();
            RebuildCode();
        }

        private void ShiftRight_Click(object sender, RoutedEventArgs e)
        {
            PushUndoBuffer();
            currentSprite.ShiftRight();
            RebuildCanvas();
            BuildSpritePreview();
            RebuildCode();
        }

        private void ShiftUp_Click(object sender, RoutedEventArgs e)
        {
            PushUndoBuffer();
            currentSprite.ShiftUp();
            RebuildCanvas();
            BuildSpritePreview();
            RebuildCode();
        }

        private void ShiftDown_Click(object sender, RoutedEventArgs e)
        {
            PushUndoBuffer();
            currentSprite.ShiftDown();
            RebuildCanvas();
            BuildSpritePreview();
            RebuildCode();
        }
    }
}