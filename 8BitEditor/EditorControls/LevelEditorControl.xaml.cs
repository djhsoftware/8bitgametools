﻿namespace EightBitGameTools.EditorControls
{
    using EightBitGameTools;
    using EightBitGameTools.Convertors;
    using EightBitGameTools.Utility;
    using Microsoft.Win32;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.Json;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using static EightBitGameTools.LevelInfo;
    using Point = System.Drawing.Point;

    /// <summary>
    /// Interaction logic for LevelEditorControl.xaml
    /// </summary>
    public partial class LevelEditorControl : UserControl, IEditorControl
    {
        private LevelInfo currentLevel;
        private ILevelConvertor levelConverter;
        private readonly Stack<string> UndoBuffer = new();
        private string lastPath;

        public LevelEditorControl()
        {
            InitializeComponent();
            levelConverter = new LevelConvertorBBC();

            // Configure BBC Sprite convertor for now
            ClearCurrentLevel();
        }

        private void MenuClear_Click(object sender, RoutedEventArgs e)
        {
            ClearCurrentLevel();
        }

        private void MenuLoad_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }

        private void MenuSave_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void MenuSaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveAs();
        }

        private void LevelName_TextChanged(object sender, TextChangedEventArgs e)
        {
            currentLevel.Name = LevelName.Text;
            RebuildCode();
        }

        private void gridCanvas_MouseDown(object sender, GridCanvasEventArgs data)
        {
            gridCanvas.SetCellSelector(data.Cell);
            SetRoom(data.Cell);
        }

        #region Common Interface Actions
        public void ClearUndoBuffer()
        {
        }

        public void PushUndoBuffer()
        {
        }

        public void PopUndoBuffer()
        {
        }

        public void Save()
        {
            if (!currentLevel.Save())
            {
                SaveAs();
            }

            // Prevent clicking on the grid while double clicking the dialog...
            gridCanvas.LockCanvas();
        }

        public void Load()
        {
            ClearUndoBuffer();
            OpenFileDialog openFileDialog = new();
            openFileDialog.Filter = "Level file (*.8bl)|*.8bl|All files (*.*)|*.*";
            if (lastPath != null) openFileDialog.InitialDirectory = lastPath;
            if (!openFileDialog.ShowDialog() ?? false) return;
            lastPath = Path.GetDirectoryName(openFileDialog.FileName);

            // Prevent clicking on the grid while double clicking the dialog...
            gridCanvas.LockCanvas();
            currentLevel = LevelInfo.Load(openFileDialog.FileName);
            LevelName.Text = currentLevel.Name;

            RebuildCanvas();
        }

        public void SaveAs()
        {
            SaveFileDialog saveFileDialog = new();
            saveFileDialog.Filter = "Level file (*.8bl)|*.8bl";
            saveFileDialog.AddExtension = true;
            if (!saveFileDialog.ShowDialog() ?? false) return;
            currentLevel.Save(saveFileDialog.FileName);
            LevelName.Text = currentLevel.Name;

            // Prevent clicking on the grid while double clicking the dialog...
            gridCanvas.LockCanvas();
        }
        #endregion

        private void IsRoomToggle_Click(object sender, RoutedEventArgs e)
        {
            var roominfo = currentLevel.GetRoomInfo(gridCanvas.CurrentCell);
            if (roominfo == null)
            {
                currentLevel.AddRoom(gridCanvas.CurrentCell);
            }
            else
            {
                currentLevel.DeleteRoom(roominfo);
            }

            SetRoom(gridCanvas.CurrentCell);
            RebuildCanvas();
        }

        private void SetRoom(Point cell)
        {
            var roominfo = currentLevel.GetRoomInfo(cell);
            var cellIndex = cell.Y * gridCanvas.Dimensions.X + cell.X;

            ActiveCellInfo.Content = $"{cell.Y}:{cell.X} [ {cellIndex} / {cellIndex:X2} ]";
            if (roominfo != null)
            {
                IsRoomToggle.Content = "-";
                RoomInfoPanel.Visibility = Visibility.Visible;
                M1Slider.Value = roominfo.Monsters[0];
                M2Slider.Value = roominfo.Monsters[1];
                M3Slider.Value = roominfo.Monsters[2];
                M4Slider.Value = roominfo.Monsters[3];
                FlagKey.Opacity = roominfo.Flags.HasFlag(LevelFlags.Key) ? 1.0 : 0.3;
                FlagExit.Opacity = roominfo.Flags.HasFlag(LevelFlags.Exit) ? 1.0 : 0.3;
                FlagLife.Opacity = roominfo.Flags.HasFlag(LevelFlags.Life) ? 1.0 : 0.3;
                FlagGem.Opacity = roominfo.Flags.HasFlag(LevelFlags.Gem) ? 1.0 : 0.3;

            }
            else
            {
                IsRoomToggle.Content = "+";
                RoomInfoPanel.Visibility = Visibility.Hidden;
            }
        }

        private void ClearCurrentLevel()
        {
            PushUndoBuffer();
            currentLevel = new LevelInfo(8, 8);

            gridCanvas.Dimensions = currentLevel.Dimensions;
            gridCanvas.Clear();

            RebuildCode();
        }

        private void RebuildCode()
        {
            CodeOutput.Text = levelConverter.BuildCode(currentLevel);
        }

        /// <summary>
        /// Rebuild the canvas grid array
        /// </summary>
        private void RebuildCanvas()
        {
            gridCanvas.Clear(Colors.Black);

            foreach (var roomInfo in currentLevel.LevelRooms)
            {
                Color colour;

                if (roomInfo.Flags.HasFlag(LevelFlags.Key))
                    colour = Colors.Red;
                else if (roomInfo.Flags.HasFlag(LevelFlags.Exit))
                    colour = Colors.Blue;
                else if (roomInfo.Flags.HasFlag(LevelFlags.Gem))
                    colour = Colors.Green;
                else if (roomInfo.Flags.HasFlag(LevelFlags.Life))
                    colour = Colors.Yellow;
                else
                    colour = Colors.Beige;

                gridCanvas.AddOrUpdateRect(roomInfo.Location, colour);
            }

            // Rebuild the code
            RebuildCode();
        }

        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var grid = ((Grid)sender);
            grid.ColumnDefinitions[0].Width = new GridLength(grid.ActualHeight);
        }

        private void FlagToggle_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            LevelFlags flag = (LevelFlags)Enum.Parse(typeof(LevelFlags), (string)button.Tag);
            button.Opacity = currentLevel.ToggleFlag(gridCanvas.CurrentCell, flag) ? 1 : 0.3;
            RebuildCanvas();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var roominfo = currentLevel.GetRoomInfo(gridCanvas.CurrentCell);
            if (roominfo == null) return;

            var index = int.Parse((string)((Slider)sender).Tag);
            roominfo.Monsters[index] = (byte)e.NewValue;
            RebuildCanvas();
        }
    }
}
