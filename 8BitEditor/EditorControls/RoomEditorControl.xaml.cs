﻿namespace EightBitGameTools.EditorControls
{
    using EightBitGameTools;
    using EightBitGameTools.Convertors;
    using EightBitGameTools.Utility;
    using Microsoft.Win32;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.Json;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using Point = System.Drawing.Point;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class RoomEditorControl : UserControl, IEditorControl
    {
        private RoomInfo currentRoom;
        private readonly IRoomConvertor roomConverter;
        private readonly Stack<string> UndoBuffer = new();
        private byte activeSpriteIndex = 0;
        private string lastPath = null;

        public RoomEditorControl()
        {
            InitializeComponent();

            DataContext = currentRoom;

            // Configure BBC room convertor for now
            roomConverter = new RoomConvertorBBC();
            ClearCurrentRoom();
        }

        #region Canvas handlers
        /// <summary>
        /// Rebuild the canvas grid array
        /// </summary>
        private void RebuildCanvas()
        {
            gridCanvas.Background = Brushes.Black;

            gridCanvas.Clear();

            for (int y = 0; y < currentRoom.Dimensions.Y; y++)
            {
                for (int x = 0; x < currentRoom.Dimensions.X; x++)
                {
                    byte val = currentRoom.GridArray[y][x];
                    Point point = new(x, y);
                    SetCellSprite(point, val, true);
                }
            }

            // Set preview image
            BuildSpritePreview();

            // Rebuild the code
            RebuildCode();
        }

        private void ClearCellSprite(Point point, byte index)
        {
            gridCanvas.ClearCell(point);
            currentRoom.GridArray[point.Y][point.X] = 0;
        }

        private void BuildSpritePreview()
        {
            SpritePreview.Source = gridCanvas.GetImageSource();
        }

        private void SetCellSprite(Point point, byte index, bool deferBuild = false)
        {
            var spriteInfo = currentRoom.Palette.GetSprite(index);
            if (spriteInfo == null) return;

            var spriteBitmap = spriteInfo.Bitmap;
            gridCanvas.AddOrUpdateBitmap(point, spriteBitmap);

            // Set colour in grid array
            currentRoom.GridArray[point.Y][point.X] = index;

            // Rebuild the code
            if (!deferBuild)
            {
                RebuildCode();
            }
        }

        private void gridCanvas_MouseMove(object sender, GridCanvasEventArgs e)
        {
            var location = e.Cell.Y * gridCanvas.Dimensions.X + e.Cell.X;
            Coords.Text = $"{e.Cell.X}:{e.Cell.Y} - {location} / {location:X2}";
            if (((MouseEventArgs)e.EventArgs).LeftButton == MouseButtonState.Pressed)
            {
                SetCellSprite(e.Cell, activeSpriteIndex);
            }
        }

        private void gridCanvas_MouseDown(object sender, GridCanvasEventArgs e)
        {
            PushUndoBuffer();
            SetCellSprite(e.Cell, activeSpriteIndex);
        }

        private void gridCanvas_GridMouseUp(object sender, GridCanvasEventArgs data)
        {
            BuildSpritePreview();
        }
        #endregion

        #region Toolbar handlers

        /// <summary>
        /// Create or update the palette sprite
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Palette_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            var button = (Button)sender;
            var spriteInfo = LoadSpriteFromFile();
            if (spriteInfo == null) return;
            var paletteIndex = (byte)button.Tag;
            currentRoom.Palette.SetSprite(paletteIndex, spriteInfo);
            Image image = new()
            {
                Source = BitmapConverter.ImageSourceFromBitmap(spriteInfo.Bitmap)
            };

            button.Content = image;
            button.ToolTip = spriteInfo.Name;

            RebuildCanvas();
        }

        private void PaletteNewSelect_Click(object sender, RoutedEventArgs e)
        {
            var spriteInfo = LoadSpriteFromFile();
            if (spriteInfo == null) return;

            // Add to sprite palette
            var spriteIndex = currentRoom.Palette.AddSprite(spriteInfo);

            CreateSpriteButton(spriteIndex, spriteInfo);
        }


        private void RebuildPalette()
        {
            for (int i = 0; i < SpritePalette.Children.Count;)
            {
                var child = SpritePalette.Children[i] as Button;
                if (child != null && child.Tag != null)
                {
                    SpritePalette.Children.Remove(child);
                }
                else
                {
                    i++;
                }
            }

            foreach (var sprite in currentRoom.Palette.SpriteInfo)
            {
                CreateSpriteButton(sprite.Key, sprite.Value);
            }
        }

        private void CreateSpriteButton(byte spriteIndex, SpriteInfo spriteInfo)
        {
            Image image = new()
            {
                Margin = new Thickness(0),
                Source = BitmapConverter.ImageSourceFromBitmap(spriteInfo.Bitmap)
            };

            // Add button to sprite palette
            var newButton = new Button
            {
                Width = PaletteNew.Width,
                Height = PaletteNew.Height,
                Tag = spriteIndex,
                Margin = PaletteNew.Margin,
                Padding = new Thickness(0),
                ToolTip = spriteInfo.Name,
                Style = (Style)Resources["NoHighlight"]
            };

            newButton.MouseRightButtonUp += Palette_MouseRightButtonUp;
            newButton.Click += PaletteSelect_Click;
            newButton.Content = image;

            // Insert into palette
            SpritePalette.Children.Insert(SpritePalette.Children.Count - 1, newButton);
        }

        private void PaletteClearSelect_Click(object sender, RoutedEventArgs e)
        {
            activeSpriteIndex = 0;
        }

        private void PaletteSelect_Click(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            activeSpriteIndex = (byte)button.Tag;
        }

        private void MenuLoad_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }

        private void MenuSaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveAs();
        }

        private void MenuSave_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void MenuClear_Click(object sender, RoutedEventArgs e)
        {
            ClearCurrentRoom();
        }
        #endregion
        private SpriteInfo? LoadSpriteFromFile()
        {
            OpenFileDialog openFileDialog = new();
            openFileDialog.Filter = "Sprite file (*.8bs)|*.8bs|All files (*.*)|*.*";
            if (lastPath != null) openFileDialog.InitialDirectory = lastPath;
            if (!openFileDialog.ShowDialog() ?? false) return null;
            lastPath = Path.GetDirectoryName(openFileDialog.FileName);
            gridCanvas.LockCanvas();
            return SpriteInfo.Load(openFileDialog.FileName);
        }

        private void SetRoomFilename(string filename)
        {
            if (currentRoom.Name == null) currentRoom.Name = System.IO.Path.GetFileNameWithoutExtension(filename);
            currentRoom.Filename = filename;
            RoomName.Text = currentRoom.Name;
        }

        private void ClearCurrentRoom()
        {
            PushUndoBuffer();
            currentRoom = new RoomInfo(20, 12, roomConverter.GetDefaultPalette());
            gridCanvas.Dimensions = currentRoom.Dimensions;
            gridCanvas.Clear();

            BuildSpritePreview();
            RebuildCode();
        }

        private void RebuildCode()
        {
            CodeOutput.Text = roomConverter.BuildCode(currentRoom);
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Z && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                PopUndoBuffer();
            }
        }

        #region Common Interface Actions
        public void ClearUndoBuffer()
        {
            UndoBuffer.Clear();
            StatusMessage.Text = UndoBuffer.Count.ToString();
        }

        public void PushUndoBuffer()
        {
            UndoBuffer.Push(JsonSerializer.Serialize(currentRoom));
            StatusMessage.Text = UndoBuffer.Count.ToString();
        }

        public void PopUndoBuffer()
        {
            if (UndoBuffer.Count == 0) return;
            var roomData = UndoBuffer.Pop();
            currentRoom = JsonSerializer.Deserialize<RoomInfo>(roomData);
            StatusMessage.Text = UndoBuffer.Count.ToString();
            RebuildCanvas();
        }

        public void Save()
        {
            if (currentRoom.Filename == null)
            {
                SaveAs();
                return;
            }

            // Prevent clicking on the grid while double clicking the dialog...
            gridCanvas.LockCanvas();

            var json = JsonSerializer.Serialize(currentRoom);
            File.WriteAllText(currentRoom.Filename, json);
        }

        public void Load()
        {
            ClearUndoBuffer();
            OpenFileDialog openFileDialog = new();
            openFileDialog.Filter = "Room file (*.8br)|*.8br|All files (*.*)|*.*";
            if (lastPath != null) openFileDialog.InitialDirectory = lastPath;
            if (!openFileDialog.ShowDialog() ?? false) return;
            lastPath = Path.GetDirectoryName(openFileDialog.FileName);

            // Prevent clicking on the grid while double clicking the dialog...
            gridCanvas.LockCanvas();

            string json = File.ReadAllText(openFileDialog.FileName);
            currentRoom = JsonSerializer.Deserialize<RoomInfo>(json);
            if (currentRoom == null)
            {
                MessageBox.Show("Room load failed", "LOAD FAILED", MessageBoxButton.OK, MessageBoxImage.Error);
                currentRoom = new RoomInfo(20, 12, roomConverter.GetDefaultPalette());
            }
            else
            {
                SetRoomFilename(openFileDialog.FileName);
            }

            RebuildPalette();
            RebuildCanvas();
        }

        public void SaveAs()
        {
            SaveFileDialog saveFileDialog = new();
            saveFileDialog.Filter = "Room file (*.8br)|*.8br";
            saveFileDialog.AddExtension = true;
            if (!saveFileDialog.ShowDialog() ?? false) return;
            var json = JsonSerializer.Serialize(currentRoom);
            SetRoomFilename(saveFileDialog.FileName);
            File.WriteAllText(currentRoom.Filename, json);

            // Prevent clicking on the grid while double clicking the dialog...
            gridCanvas.LockCanvas();
        }
        #endregion

        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var grid = ((Grid)sender);
            double ratio = (double)currentRoom.Dimensions.X / (double)currentRoom.Dimensions.Y;
            grid.ColumnDefinitions[0].Width = new GridLength(grid.ActualHeight * ratio);

        }

        private void RoomName_TextChanged(object sender, TextChangedEventArgs e)
        {
            currentRoom.Name = RoomName.Text;
            RebuildCode();
        }

        private void MenuImport_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MenuExport_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
