﻿using EightBitGameTools.Convertors;
using Microsoft.Win32;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace EightBitGameTools.EditorControls
{
    /// <summary>
    /// Interaction logic for TextditorControl.xaml
    /// </summary>
    public partial class TextEditorControl : UserControl
    {
        ITextConvertor textConvertor = new TextConvertorBBCBasicSave();
        private string lastPath = null;
        private string lastFileName = null;

        public TextEditorControl()
        {
            InitializeComponent();
        }

        private void MenuSaveAs_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new();
            saveFileDialog.AddExtension = false;
            if (!saveFileDialog.ShowDialog() ?? false) return;

            textConvertor.Save(TextEditor.Text, saveFileDialog.FileName);
            lastFileName = saveFileDialog.FileName;
        }

        private void MenuClear_Click(object sender, RoutedEventArgs e)
        {
            TextEditor.Text = "";
        }

        private void MenuLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new();
            if (lastPath != null) openFileDialog.InitialDirectory = lastPath;
            if (!openFileDialog.ShowDialog() ?? false) return;

            TextEditor.Text = textConvertor.Load(openFileDialog.FileName);

            lastPath = Path.GetDirectoryName(openFileDialog.FileName);
            lastFileName = openFileDialog.FileName;
        }

        private void MenuSave_Click(object sender, RoutedEventArgs e)
        {
            if (lastFileName == null) MenuSaveAs_Click(sender, e);
            else
            {
                textConvertor.Save(TextEditor.Text, lastFileName);
            }
        }
    }
}
