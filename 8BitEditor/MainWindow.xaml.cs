﻿namespace EightBitGameTools
{
    using EightBitGameTools;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                var item = (TabItem)TabControl.SelectedItem;
                var editor = item.Content as IEditorControl;
                if (editor == null) return;

                if (e.Key == Key.Z)             // UNDO
                {
                    editor.PopUndoBuffer();
                }
                else if (e.Key == Key.S)        // SAVE
                {
                    if ((Keyboard.Modifiers & ModifierKeys.Shift) == ModifierKeys.Shift)
                    {
                        editor.SaveAs();
                    }
                    else
                    {
                        editor.Save();
                    }
                }
                else if (e.Key == Key.O)        // LOAD
                {
                    editor.Load();
                }
            }
        }
    }
}
