﻿namespace EightBitGameTools
{
    public interface IEditorControl
    {
        void PushUndoBuffer();

        void PopUndoBuffer();

        void Save();

        void Load();

        void SaveAs();
    }
}
