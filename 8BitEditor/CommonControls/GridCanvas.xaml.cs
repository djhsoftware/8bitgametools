﻿namespace EightBitGameTools
{
    using EightBitGameTools.Utility;
    using System;
    using System.ComponentModel;
    using System.Timers;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using Point = System.Drawing.Point;

    /// <summary>
    /// Interaction logic for GridCanvas.xaml
    /// </summary>
    public partial class GridCanvas : UserControl
    {
        public Point Dimensions { get; set; }

        public Point CurrentCell { get; private set; }

        public delegate void GridCanvasEventHandler(object sender, GridCanvasEventArgs data);

        public event GridCanvasEventHandler GridMouseDown;
        public event GridCanvasEventHandler GridMouseRightClick;
        public event GridCanvasEventHandler GridMouseMove;
        public event GridCanvasEventHandler GridMouseUp;

        private double cellHeight;
        private double cellWidth;
        private readonly Timer loadLockoutTimer = new(500);

        public GridCanvas()
        {
            InitializeComponent();
            selector.Tag = new Point(0, 0);
            loadLockoutTimer.Elapsed += loadLockoutTimerElapsed;
        }

        /// <summary>
        /// When performing click actions
        /// Temporarily lock the canvas to prevent click through
        /// </summary>
        public void LockCanvas()
        {
            canvas.IsEnabled = false;
            loadLockoutTimer.Start();
        }

        public void Clear()
        {
            cellHeight = canvas.ActualHeight / Dimensions.Y;
            cellWidth = canvas.ActualWidth / Dimensions.X;
            canvas.Children.Clear();
            canvas.Children.Add(selector);
        }

        public void Clear(Color color)
        {
            canvas.Background = new SolidColorBrush(color);
            Clear();
        }

        public void SetCellSelector(Point point)
        {
            selector.Visibility = Visibility.Visible;
            selector.Tag = point;
            selector.Width = cellWidth;
            selector.Height = cellHeight;
            Canvas.SetLeft(selector, point.X * cellWidth);
            Canvas.SetTop(selector, point.Y * cellHeight);
            CurrentCell = point;
        }

        public void ClearCellSelector()
        {
            selector.Visibility = Visibility.Hidden;
        }

        public void ClearCell(Point point)
        {
            FrameworkElement? cell = GetCellAtPoint<FrameworkElement>(point);
            if (cell != null) canvas.Children.Remove(cell);
        }

        public Image? AddOrUpdateBitmap(Point point, System.Drawing.Bitmap bitmap)
        {
            Image? cell = GetCellAtPoint<Image>(point);
            if (cell == null)
            {
                var cellName = $"CELL{point.X}_{point.Y}";
                var image = BitmapConverter.BitmapToImage(bitmap);
                image.Width = cellWidth;
                image.Height = cellHeight;
                Canvas.SetLeft(image, point.X * cellWidth);
                Canvas.SetTop(image, point.Y * cellHeight);
                image.Tag = point;
                image.Name = cellName;
                canvas.Children.Add(image);
            }
            else
            {
                cell.Source = BitmapConverter.ImageSourceFromBitmap(bitmap);
            }

            return cell;
        }

        public void AddOrUpdateRect(Point point, Color colour)
        {
            Rectangle? cell = GetCellAtPoint<Rectangle>(point);
            if (cell == null)
            {
                var cellName = $"CELL{point.X}_{point.Y}";
                var rect = new Rectangle
                {
                    Tag = point,
                    Name = cellName,
                    Stroke = new SolidColorBrush(colour),
                    Fill = new SolidColorBrush(colour),
                    Width = cellWidth,
                    Height = cellHeight
                };

                Canvas.SetLeft(rect, point.X * cellWidth);
                Canvas.SetTop(rect, point.Y * cellHeight);
                canvas.Children.Add(rect);
            }
            else
            {
                cell.Stroke = new SolidColorBrush(colour);
                cell.Fill = new SolidColorBrush(colour);
            }
        }

        internal ImageSource GetImageSource()
        {
            // To prevent errors wile initialising
            if (canvas.ActualWidth == 0) return new BitmapImage();

            //SpritePreview.Source = BitmapConverter.BitmapToImageSource(currentSprite.Bitmap());
            return BitmapConverter.CanvasToImageSource(canvas);
        }

        public void SetBackgroundColour(Color color)
        {
            canvas.Background = new SolidColorBrush(color);
        }

        public bool HasCellAtPoint(Point point)
        {
            var cellName = $"CELL{point.X}_{point.Y}";
            return UIElementExtensions.FindControl<FrameworkElement>(canvas, cellName) != null;
        }

        public T? GetCellAtPoint<T>(Point point) where T : FrameworkElement
        {
            var cellName = $"CELL{point.X}_{point.Y}";
            return UIElementExtensions.FindControl<T>(canvas, cellName);
        }

        private void GridCanvas_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var coords = GetActiveCanvasCell((Canvas)sender);
                GridMouseDown?.Invoke(sender, new GridCanvasEventArgs(e, coords));
            }
        }

        private void GridCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            var coords = GetActiveCanvasCell((Canvas)sender);
            GridMouseMove?.Invoke(sender, new GridCanvasEventArgs(e, coords));
        }

        private void canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                var coords = GetActiveCanvasCell((Canvas)sender);
                GridMouseUp?.Invoke(sender, new GridCanvasEventArgs(e, coords));
            }
            else if (e.ChangedButton == MouseButton.Right)
            {
                var coords = GetActiveCanvasCell((Canvas)sender);
                GridMouseRightClick?.Invoke(sender, new GridCanvasEventArgs(e, coords));
            }
        }

        private void GridCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (DesignerProperties.GetIsInDesignMode(this)) return;

            cellHeight = canvas.ActualHeight / Dimensions.Y;
            cellWidth = canvas.ActualWidth / Dimensions.X;

            foreach (FrameworkElement child in canvas.Children)
            {
                child.Width = cellWidth;
                child.Height = cellHeight;
                Point coords = (Point)child.Tag;
                Canvas.SetLeft(child, coords.X * cellWidth);
                Canvas.SetTop(child, coords.Y * cellHeight);
            }
        }

        private Point GetActiveCanvasCell(Canvas canvas)
        {
            var point = Mouse.GetPosition(canvas);
            int x = (int)(point.X / cellWidth);
            int y = (int)(point.Y / cellHeight);
            if (x >= Dimensions.X) x = Dimensions.X - 1;
            if (y >= Dimensions.Y) y = Dimensions.Y - 1;

            var cell = new Point(x, y);

            return cell;
        }

        /// <summary>
        /// Prevents annoying spillover of double click on file dialog by briefly locking the canvas
        /// </summary>
        private void loadLockoutTimerElapsed(object? sender, ElapsedEventArgs e)
        {
            loadLockoutTimer.Stop();
            Dispatcher.Invoke(() => { canvas.IsEnabled = true; });
        }
    }

    public class GridCanvasEventArgs : EventArgs
    {
        public GridCanvasEventArgs(MouseEventArgs e, Point coords)
        {
            Canvas? canvas = e.Source as Canvas;
            var mpoint = Mouse.GetPosition(canvas);
            Point = new Point((int)mpoint.X, (int)mpoint.Y);
            Cell = coords;
            EventArgs = e;
        }

        /// <summary>
        /// The point on the canvas that was clicked
        /// </summary>
        public Point Point { get; set; }

        /// <summary>
        /// The grid cell on the canvas that was clicked
        /// </summary>
        public Point Cell { get; set; }

        public EventArgs EventArgs { get; set; }
    }
}
