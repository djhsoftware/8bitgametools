﻿using System.Collections.Generic;
using System.Linq;

namespace EightBitGameTools.Utility
{
    public static class LinqExtensions
    {
        public static List<List<T>> SplitInto<T>(this IEnumerable<T> source, int size)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / size)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
    }
}
