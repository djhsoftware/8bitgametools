﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace EightBitGameTools.Utility
{
    public static class EnumHelper
    {
        public static string ToStringList(this Enum value, Enum exclude)
        {
            var setValues = new List<Enum>();
            foreach (var enumValue in Enum.GetValues(value.GetType()))
            {
                if (value.HasFlag((Enum)enumValue) && !exclude.HasFlag(value)) setValues.Add((Enum)enumValue);
            }

            return string.Join(" | ", setValues);
        }
    }
}
