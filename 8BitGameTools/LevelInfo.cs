﻿namespace EightBitGameTools
{
    using EightBitGameTools.Palette;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.Json;
    using System.Text.Json.Serialization;
    using System.Windows;
    using Point = System.Drawing.Point;

    public class LevelInfo
    {
        public LevelInfo()
        {
        }

        public LevelInfo(int width, int height)
        {
            Name = "NewLevel";
            Dimensions = new Point(width, height);
        }

        [JsonIgnore]
        public string Filename { get; set; }
        public Point Dimensions { get; set; }
        public string Name { get; set; }
        public RoomPalette Palette { get; set; } = new RoomPalette();
        public List<LevelRoomInfo> LevelRooms { get; set; } = new List<LevelRoomInfo>();
        public void Clear()
        {
        }

        public void SetGridSize(Point size)
        {
            Dimensions = size;
        }

        public LevelRoomInfo GetRoomInfo(int x, int y)
        {
            return LevelRooms.FirstOrDefault(r => r.Location.X == x && r.Location.Y == y);
        }

        public LevelRoomInfo GetRoomInfo(Point cell)
        {
            return LevelRooms.FirstOrDefault(r => r.Location.X == cell.X && r.Location.Y == cell.Y);
        }

        public static LevelInfo Load(string fileName)
        {
            string json = File.ReadAllText(fileName);
            var currentLevel = JsonSerializer.Deserialize<LevelInfo>(json);
            if (currentLevel == null)
            {
                MessageBox.Show("Level load failed", "LOAD FAILED", MessageBoxButton.OK, MessageBoxImage.Error);
                currentLevel = new LevelInfo(20, 12);
            }

            foreach (var room in currentLevel.LevelRooms)
            {
                //room.Flags &= (LevelFlags.Key | LevelFlags.Gem | LevelFlags.Life | LevelFlags.Exit);
            }

            currentLevel.Filename = fileName;

            return currentLevel;
        }

        public bool Save()
        {
            if (string.IsNullOrEmpty(Filename)) return false;
            return Save(Filename);
        }

        public bool Save(string filename)
        {
            if (string.IsNullOrEmpty(filename)) return false;
            if (string.IsNullOrEmpty(Name)) if (Name == null) Name = Path.GetFileNameWithoutExtension(filename);
            Filename = filename;
            var json = JsonSerializer.Serialize(this);
            File.WriteAllText(filename, json);
            return true;
        }

        public void AddRoom(Point currentCell)
        {
            if (LevelRooms.Any(r => r.Location.X == currentCell.X && r.Location.Y == currentCell.Y)) return;
            LevelRooms.Add(new LevelRoomInfo(currentCell));
        }

        public void DeleteRoom(LevelRoomInfo room)
        {
            if (room == null) return;
            LevelRooms.Remove(room);
        }

        /// <summary>
        /// Toggle flag, if permitted
        /// </summary>
        /// <param name="currentCell">Current cell to toggle</param>
        /// <param name="flag">Flag to toggle</param>
        /// <returns>True if flag is not off, false if flag is on</returns>
        public bool ToggleFlag(Point currentCell, LevelFlags flag)
        {
            var roominfo = GetRoomInfo(currentCell);
            if (roominfo == null) return false;

            // Not allowed to remove key or exit once placed, only move it
            if (flag == LevelFlags.Exit || flag == LevelFlags.Key)
            {
                if (!roominfo.Flags.HasFlag(flag))
                {
                    var currentExitRoom = LevelRooms.FirstOrDefault(r => r.Flags.HasFlag(flag));
                    if (currentExitRoom != null) currentExitRoom.Flags -= flag;
                    roominfo.Flags |= flag;
                }
            }
            else if (roominfo.Flags.HasFlag(flag))
            {
                roominfo.Flags -= flag;
            }
            else
            {
                roominfo.Flags |= flag;
            }

            return roominfo.Flags.HasFlag(flag);
        }
    }
}