﻿namespace EightBitGameTools.Convertors
{
    using EightBitGameTools.Palette;

    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class SpriteConvertorCoco2 : ISpriteConvertor
    {
        /// <summary>
        /// The bitmask for converting values
        /// </summary>
        private readonly byte[] bitMask = { 0b00000000, 0b00000001, 0b00010000, 0b00010001 };

        /// <summary>
        /// The logical palette map for this mode
        /// </summary>
        private readonly Dictionary<string, byte> logicalPaletteMap = new()
        {
            { "#00FF00", 0 },   // GREEN
            { "#FFFF00", 1 },   // YELLOW
            { "#0000FF", 2 },   // BLUE
            { "#FF0000", 3 },   // RED
        };

        /// <summary>
        /// The name of this convertor
        /// </summary>
        public string Name => "Tandy Color Computer 2";

        /// <summary>
        /// Flag indicating that this is an indexed palette
        /// </summary>
        public bool UseIndexedPalette => true;

        /// <summary>
        /// Return the index palette values for this convertor
        /// </summary>
        string[] ISpriteConvertor.IndexedPalette => logicalPaletteMap.Keys.ToArray();

        /// <summary>
        /// Get the default palette for this convertor
        /// </summary>
        /// <returns>New palette instance</returns>
        public IndexedPalette GetDefaultPalette()
        {
            return new IndexedPalette(logicalPaletteMap.Keys.Take(4).ToArray());
        }

        /// <summary>
        /// Build the sprite data and palette info
        /// </summary>
        /// <param name="sprite">Sprite to build</param>
        /// <returns>Built code</returns>
        public string BuildCode(SpriteInfo sprite)
        {
            StringBuilder sb = new();

            // Sprite Header
            string spacer = "            ";
            int len = sprite.Dimensions.Y * sprite.Dimensions.X / 4;
            int width = sprite.Dimensions.X / 4;
            int height = sprite.Dimensions.Y;
            int hwrap = 32 - width; // 32*4 = 128 => 128x192 resolution screen
            int screenspace = (width * height) + 
                              (hwrap * (height - 1));
            
            sb.AppendLine($"{spacer}FDB #${screenspace:X4}    ; Screen space");
            sb.AppendLine($"{sprite.Name.PadRight(12)}FDB #${width:X2}{hwrap:X2}    ; Width | Wrap");
            sb.AppendLine($"{spacer}FCB #${len:X2}      ; Total Length");

            for (int gy = 0; gy < sprite.Dimensions.Y; gy++)
            {
                for (int gx = 0; gx < sprite.Dimensions.X; gx += 4)
                {
                    var c1 = sprite.GridArray[gy][gx];
                    var c2 = sprite.GridArray[gy][gx + 1];
                    var c3 = sprite.GridArray[gy][gx + 2];
                    var c4 = sprite.GridArray[gy][gx + 3];
                    int bOut = (c1 << 6) + (c2 << 4) + (c3 << 2) + c4;

                    sb.AppendLine($"{spacer}FCB #${bOut:X2}");
                    /*
                    // Sprite bytes (2 bytes per row = 16 bytes total)
                    string[] b = new string[8];
                    for (int gx = 0; gx < 8; gx += 4)
                    {
                        int bOut = 0;
                        for (int gy = 0; gy < 8; gy++)
                        {
                            var px1 = sprite.GridArray[gyStart + gy][gxStart + gx];
                            var px2 = sprite.GridArray[gyStart + gy][gxStart + gx + 1];
                            var px3 = sprite.GridArray[gyStart + gy][gxStart + gx + 2];
                            var px4 = sprite.GridArray[gyStart + gy][gxStart + gx + 3];
                            bOut = (bitMask[px1] << 3) + (bitMask[px2] << 2) + (bitMask[px3] << 1) + bitMask[px4];
                            b[gy] = "$" + bOut.ToString("X2");
                        }

                        sb.AppendLine($"\tdb {string.Join(",", b)}");
                    }*/
                }
            }

            // Map palette
            var p0 = GetPaletteIndexOrDefault(sprite.Palette[0]);
            var p1 = GetPaletteIndexOrDefault(sprite.Palette[1]);
            var p2 = GetPaletteIndexOrDefault(sprite.Palette[2]);
            var p3 = GetPaletteIndexOrDefault(sprite.Palette[3]);

            // Write palette
            sb.AppendLine("");
            //sb.AppendLine($"\tdb $0{p0},$1{p0},$4{p0},$5{p0}; {sprite.Palette[0]}");
            //sb.AppendLine($"\tdb $2{p1},$3{p1},$6{p1},$7{p1}; {sprite.Palette[1]}");
            //sb.AppendLine($"\tdb $8{p2},$9{p2},$c{p2},$d{p2}; {sprite.Palette[2]}");
            //sb.AppendLine($"\tdb $a{p3},$b{p3},$e{p3},$f{p3}; {sprite.Palette[3]}");

            // Return results
            return sb.ToString();
        }

        /// <summary>
        /// Locate the logical paletter index for the given colour, if it can't be found default to the first colour
        /// </summary>
        /// <param name="colour">Hex colour to be found</param>
        /// <returns>Logical palette index</returns>
        private byte GetPaletteIndexOrDefault(string colour)
        {
            if (logicalPaletteMap.ContainsKey(colour))
            {
                return logicalPaletteMap[colour];
            }
            else
            {
                return logicalPaletteMap.First().Value;
            }
        }
    }
}
