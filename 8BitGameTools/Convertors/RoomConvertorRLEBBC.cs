﻿namespace EightBitGameTools.Convertors
{
    using EightBitGameTools;
    using EightBitGameTools.Palette;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class RoomConvertorRLEBBC : IRoomConvertor
    {
        public string Name => "RLE Room Converter (BBC)";

        public string BuildCode(RoomInfo currentRoom)
        {
            StringBuilder sb = new();

            // Sprite Header
            sb.AppendLine($"{currentRoom.Name}:");

            for (int y = 0; y < currentRoom.Dimensions.Y; y++)
            {
                List<byte> rleBuffer = new();
                byte dx = 255;
                byte run = 0;
                for (int x = 0; x < currentRoom.Dimensions.X; x++)
                {
                    byte flags = 0;

                    // Calculate movement flags
                    if (x > 0 && currentRoom.GridArray[y][x - 1] == 0) flags += 128;                               // LEFT
                    if (x < currentRoom.Dimensions.X - 1 && currentRoom.GridArray[y][x + 1] == 0) flags += 64;    // RIGHT
                    if (y > 0 && currentRoom.GridArray[y - 1][x] == 0) flags += 32;                                // UP
                    if (y < currentRoom.Dimensions.Y - 1 && currentRoom.GridArray[y + 1][x] == 0) flags += 16;      // DOWN

                    byte cellValue = (byte)(currentRoom.GridArray[y][x] + flags);

                    // Save the first value
                    if (dx == 255)
                    {
                        dx = cellValue;
                    }
                    else if (dx != cellValue || run == 254)
                    {
                        rleBuffer.Add(run);
                        rleBuffer.Add(dx);
                        dx = cellValue;
                        run = 0;
                    }

                    run++;
                }

                // Add remaining run buffer
                if (run > 0)
                {
                    rleBuffer.Add(run);
                    rleBuffer.Add(dx);
                }
                var byteStrings = rleBuffer.Select(g => "$" + g.ToString("X2"));
                sb.AppendLine($"\tdb {string.Join(",", byteStrings)}");
            }

            // Return results
            return sb.ToString();
        }

        public SpritePalette GetDefaultPalette()
        {
            return new SpritePalette();
        }
    }
}