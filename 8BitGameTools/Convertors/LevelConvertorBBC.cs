﻿namespace EightBitGameTools
{
    using EightBitGameTools;
    using EightBitGameTools.Convertors;
    using EightBitGameTools.Utility;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class LevelConvertorBBC : ILevelConvertor
    {
        public string Name => "Level Convertor (BBC)";

        public string BuildCode(LevelInfo currentLevel)
        {
            StringBuilder sb = new();
            List<byte> lineBuffer;

            // 0 = room index | 1 = room flags
            lineBuffer = new();
            foreach (var roominfo in currentLevel.LevelRooms)
            {
                // Nothing here - don't write it
                if (roominfo.Flags == 0) continue;

                var roomIndex = (byte)(roominfo.Location.Y * currentLevel.Dimensions.X + roominfo.Location.X);

                lineBuffer.Add(roomIndex);              // Room index
                lineBuffer.Add((byte)roominfo.Flags);   // Room flags
            }

            // End of data
            lineBuffer.Add(0xff);

            sb.AppendLine($"{currentLevel.Name}Flags:");
            var strings = lineBuffer.Select(g => "$" + g.ToString("X2")).SplitInto(8);
            foreach (var stringlist in strings)
            {
                sb.AppendLine($"\tdb {string.Join(",", stringlist)}");
            }

            sb.AppendLine();

            // monster 4 | monster 3 | monster 2 | monster 1 (2 bits each - up to 3 monsters of each type)
            lineBuffer = new();
            foreach (var roominfo in currentLevel.LevelRooms)
            {
                // Nothing here - don't write it
                if (roominfo.Monsters[0] == 0 && roominfo.Monsters[1] == 0 &&
                    roominfo.Monsters[2] == 0 && roominfo.Monsters[3] == 0) continue;

                var roomIndex = (byte)(roominfo.Location.Y * currentLevel.Dimensions.X + roominfo.Location.X);

                byte monsterFlags = (byte)(roominfo.Monsters[0] +
                                    (byte)(roominfo.Monsters[1] << 2) +
                                    (byte)(roominfo.Monsters[2] << 4) +
                                    (byte)(roominfo.Monsters[3] << 6));

                lineBuffer.Add(roomIndex);              // Room index
                lineBuffer.Add(monsterFlags);           // Monster info
            }

            // End of data
            lineBuffer.Add(0xff);   

            sb.AppendLine($"{currentLevel.Name}Monsters:");
            strings = lineBuffer.Select(g => "$" + g.ToString("X2")).SplitInto(8);
            foreach (var stringlist in strings)
            {
                sb.AppendLine($"\tdb {string.Join(",", stringlist)}");
            }

            sb.AppendLine();

            // Sprite Header
            sb.AppendLine($"{currentLevel.Name}Map:");

            // TEMPORARY
            int seed = currentLevel.Filename?.GetHashCode() ?? (int)DateTime.UtcNow.Ticks;
            Random rnd = new(seed);

            // Setup grid Array
            var gridArray = new byte[currentLevel.Dimensions.Y][];
            for (int y = 0; y < currentLevel.Dimensions.Y; y++)
            {
                gridArray[y] = new byte[currentLevel.Dimensions.X];
                for (int x = 0; x < currentLevel.Dimensions.X; x++)
                {
                    var roominfo = currentLevel.GetRoomInfo(x, y);
                    gridArray[y][x] = (byte)(roominfo == null ? 0 : 1);
                }
            }

            // LEVEL DATA
            for (int y = 0; y < currentLevel.Dimensions.Y; y++)
            {
                lineBuffer = new();
                for (int x = 0; x < currentLevel.Dimensions.X; x++)
                {
                    byte flags = 0;
                    if (gridArray[y][x] > 0)
                    {
                        // LSB - RoomType
                        flags = (byte)(rnd.Next(0, 15));

                        // MSB - Navigation
                        if (x > 0 && gridArray[y][x - 1] != 0) flags += 128;                              // LEFT
                        if (x < currentLevel.Dimensions.X - 1 && gridArray[y][x + 1] != 0) flags += 64;   // RIGHT
                        if (y > 0 && gridArray[y - 1][x] != 0) flags += 32;                               // UP
                        if (y < currentLevel.Dimensions.Y - 1 && gridArray[y + 1][x] != 0) flags += 16;   // DOWN
                    }

                    lineBuffer.Add(flags);
                }

                var byteStrings = lineBuffer.Select(g => "$" + g.ToString("X2"));
                sb.AppendLine($"\tdb {string.Join(",", byteStrings)}");
            }

            // Return results
            return sb.ToString();
        }
    }
}