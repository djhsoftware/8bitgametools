﻿namespace EightBitGameTools.Convertors
{
    using EightBitGameTools.Palette;

    public interface IRoomConvertor
    {
        string Name { get; }

        string BuildCode(RoomInfo currentLevel);
        SpritePalette GetDefaultPalette();
    }
}