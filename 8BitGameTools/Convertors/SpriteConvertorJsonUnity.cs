﻿using EightBitGameTools.Palette;

using System;

namespace EightBitGameTools.Convertors
{
    internal class SpriteConvertorJsonUnity : ISpriteConvertor
    {
        public string Name => "Json (Unity)";

        public bool UseIndexedPalette => false;

        public string[] IndexedPalette => throw new NotImplementedException();

        public string BuildCode(SpriteInfo sprite)
        {
            throw new NotImplementedException();
        }

        public IndexedPalette GetDefaultPalette()
        {
            throw new NotImplementedException();
        }
    }
}
