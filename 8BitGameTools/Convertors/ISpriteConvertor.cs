﻿namespace EightBitGameTools.Convertors
{
    using EightBitGameTools;
    using EightBitGameTools.Palette;

    public interface ISpriteConvertor
    {
        /// <summary>
        /// Name of this sprite convertor
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Does this convertor use an indexed palette
        /// </summary>
        bool UseIndexedPalette { get; }

        /// <summary>
        /// Indexed palette
        /// </summary>
        string[] IndexedPalette { get; }

        /// <summary>
        /// The code builder
        /// </summary>
        /// <param name="sprite">Sprite to build</param>
        /// <returns>Generated code</returns>
        string BuildCode(SpriteInfo sprite);
        IndexedPalette GetDefaultPalette();
    }
}