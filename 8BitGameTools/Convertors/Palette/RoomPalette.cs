﻿namespace EightBitGameTools.Palette
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.Json.Serialization;

    public class RoomPalette : PaletteBase
    {
        /// <summary>
        /// Zero is reserved in sprite index (0 = no sprite)
        /// </summary>
        [JsonIgnore]
        public SortedList<byte, RoomInfo> RoomInfo = new();

        public List<RoomData> SpriteList
        {
            get
            {
                return RoomInfo.Select(s => new RoomData()
                {
                    Filename = s.Value.Filename,
                    Index = s.Key
                }).ToList();
            }
            set
            {
                foreach (var roomdata in value)
                {
                    RoomInfo.Add(roomdata.Index, EightBitGameTools.RoomInfo.Load(roomdata.Filename));
                }
            }
        }

        public RoomInfo? GetRoom(byte index)
        {
            if (index == 0 || index > RoomInfo.Count) return null;
            return RoomInfo[index];
        }

        public byte AddRoom(RoomInfo roomInfo)
        {
            byte spriteIndex = (byte)(RoomInfo.Count + 1);
            RoomInfo.Add(spriteIndex, roomInfo);
            return spriteIndex;
        }

        public void SetRoom(byte spriteIndex, RoomInfo roomInfo)
        {
            if (spriteIndex == 0) return;
            RoomInfo[spriteIndex] = roomInfo;
        }

        public class RoomData
        {
            public byte Index { get; set; }
            public string Filename { get; set; }
        }
    }
}
