﻿namespace EightBitGameTools.Palette
{
    using System.Windows.Media;

    public class IndexedPalette : PaletteBase
    {
        public string[] PaletteValues { get; set; }
        public int Size => PaletteValues.Length;

        public string this[int index]
        {
            get
            {
                return PaletteValues[index];
            }
            set
            {
                PaletteValues[index] = value;
            }
        }

        public IndexedPalette()
        {
        }

        public IndexedPalette(int indexes)
        {
            PaletteValues = new string[indexes];
        }

        public IndexedPalette(string[] values)
        {
            PaletteValues = values;
        }

        public static IndexedPalette Create(string[] palette)
        {
            return new IndexedPalette(palette.Length)
            {
                PaletteValues = palette
            };
        }

        public Color GetColor(int index)
        {
            if (index > PaletteValues.Length - 1)
                return (Color)ColorConverter.ConvertFromString(PaletteValues[0]);
            else
                return (Color)ColorConverter.ConvertFromString(PaletteValues[index]);
        }
    }
}
