﻿namespace EightBitGameTools.Palette
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.Json.Serialization;

    public class SpritePalette : PaletteBase
    {
        /// <summary>
        /// Zero is reserved in sprite index (0 = no sprite)
        /// </summary>
        [JsonIgnore]
        public SortedList<byte, SpriteInfo> SpriteInfo = new();

        public List<SpriteData> SpriteList
        {
            get
            {
                return SpriteInfo.Select(s => new SpriteData()
                {
                    Filename = s.Value.Filename,
                    Index = s.Key
                }).ToList();
            }
            set
            {
                foreach (var spritedata in value)
                {
                    SpriteInfo.Add(spritedata.Index, EightBitGameTools.SpriteInfo.Load(spritedata.Filename));
                }
            }
        }

        public SpriteInfo? GetSprite(byte index)
        {
            if (index > SpriteInfo.Count || index == 0) return null;
            return SpriteInfo[index];
        }

        public void RebuildBitmaps()
        {
            foreach (var sprite in SpriteInfo)
            {
                sprite.Value.UpdateBitmap();
            }
        }
        public byte AddSprite(SpriteInfo spriteInfo)
        {
            byte spriteIndex = (byte)(SpriteInfo.Count);
            SpriteInfo.Add(spriteIndex, spriteInfo);
            return spriteIndex;
        }

        public void SetSprite(byte spriteIndex, SpriteInfo spriteInfo)
        {
            if (spriteIndex == 0) return;
            SpriteInfo[spriteIndex] = spriteInfo;
        }

        public class SpriteData
        {
            public byte Index { get; set; }
            public string Filename { get; set; }
        }

        public void RemoveSprite(byte paletteIndex)
        {
            SpriteInfo.Remove(paletteIndex);
        }
    }
}
