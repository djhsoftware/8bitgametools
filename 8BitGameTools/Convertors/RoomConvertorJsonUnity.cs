﻿namespace EightBitGameTools.Convertors
{
    using EightBitGameTools;
    using EightBitGameTools.Palette;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class RoomConvertorJsonUnity : IRoomConvertor
    {
        public string Name => "Room Convertor (BBC)";

        public string BuildCode(RoomInfo currentRoom)
        {
            StringBuilder sb = new();

            // Sprite Header
            sb.AppendLine($"{currentRoom.Name}:");

            for (int y = 0; y < currentRoom.Dimensions.Y; y++)
            {
                List<byte> roomBuffer = new();
                for (int x = 0; x < currentRoom.Dimensions.X; x++)
                {
                    byte flags = 0;
                    if (currentRoom.GridArray[y][x] == 0)
                    {
                        // Calculate movement flags
                        if (x > 0 && currentRoom.GridArray[y][x - 1] == 0) flags += 128;                               // LEFT
                        if (x < currentRoom.Dimensions.X - 1 && currentRoom.GridArray[y][x + 1] == 0) flags += 64;    // RIGHT
                        if (y > 0 && currentRoom.GridArray[y - 1][x] == 0) flags += 32;                                // UP
                        if (y < currentRoom.Dimensions.Y - 1 && currentRoom.GridArray[y + 1][x] == 0) flags += 16;      // DOWN
                    }

                    byte cellValue = (byte)(currentRoom.GridArray[y][x] + flags);
                    roomBuffer.Add(cellValue);
                }

                var byteStrings = roomBuffer.Select(g => "$" + g.ToString("X2"));
                sb.AppendLine($"\tdb {string.Join(",", byteStrings)}");
            }

            // Return results
            return sb.ToString();
        }

        public SpritePalette GetDefaultPalette()
        {
            return new SpritePalette();
        }
    }
}