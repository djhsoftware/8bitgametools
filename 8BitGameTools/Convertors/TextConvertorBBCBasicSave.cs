﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace EightBitGameTools.Convertors
{
    public class TextConvertorBBCBasicSave : ITextConvertor
    {
        public string Load(string filename)
        {
            string lineKey = "";
            int counter = 0;
            int lineNo = 0;
            StringBuilder sb = new StringBuilder();
            using (StreamReader sr = new StreamReader(filename))
            {
                List<int> buffer = new List<int>();
                while (!sr.EndOfStream)
                {
                    var ch = sr.Read();
                    if (ch == 0x0D)
                    {
                        counter++;
                        if (buffer.Count > 0)
                        {
                            var codeLine = string.Join("", buffer.Select(b => Convert.ToChar(b)));
                            sb.AppendLine($"[{lineNo}]\t{codeLine}");
                        }

                        // Reset line buffer
                        buffer = new List<int>();

                        // Read the next three bytes (line marker?)
                        var ln1 = sr.Read();
                        if (ln1 == 0xFF) break;  // END OF FILE
                        var ln2 = sr.Read();
                        lineNo = (ln1 << 8) + ln2;
                        var ch2 = sr.Read();
                        lineKey = $"{lineNo} {ch2:X2}";
                    }
                    else
                    {
                        buffer.Add(ch);
                    }
                }
            }

            return sb.ToString();
        }

        public void Save(string content, string filename)
        {
        }
    }
}
