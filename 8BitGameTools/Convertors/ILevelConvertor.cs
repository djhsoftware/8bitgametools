﻿namespace EightBitGameTools.Convertors
{
    public interface ILevelConvertor
    {
        string Name { get; }

        string BuildCode(LevelInfo currentLevel);
    }
}