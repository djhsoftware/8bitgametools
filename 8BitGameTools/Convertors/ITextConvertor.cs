﻿namespace EightBitGameTools.Convertors
{
    public interface ITextConvertor
    {
        void Save(string text, string fileName);

        string Load(string filename);
    }
}
