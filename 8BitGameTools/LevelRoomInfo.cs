﻿namespace EightBitGameTools
{
    using Point = System.Drawing.Point;

    public enum LevelFlags
    {
        None = 0,
        Key = 1,
        Gem = 2,
        Life = 4,
        Exit = 128
    }

    public class LevelRoomInfo
    {
        public LevelRoomInfo()
        {
        }

        public LevelRoomInfo(Point cell)
        {
            Location = cell;
        }

        public LevelRoomInfo(int x, int y)
        {
            Location = new Point(x, y);
        }

        public Point Location { get; set; }
        public LevelFlags Flags { get; set; }
        public int TileSetIndex { get; set; }
        public byte[] Monsters { get; set; } = new byte[6];
    }
}
