﻿namespace EightBitGameTools
{
    using EightBitGameTools.Palette;
    using EightBitGameTools.Utility;

    using Microsoft.Win32;

    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Text.Json;
    using System.Text.Json.Serialization;
    using System.Windows.Controls;

    using Image = System.Drawing.Image;

    public class RoomInfo
    {
        private Bitmap bitmap = null;
        private static string lastPath = null;
        public RoomInfo()
        {
        }

        public RoomInfo(int width, int height, SpritePalette palette)
        {
            Palette = palette;
            Name = "NewRoom";
            Dimensions = new Point(width, height);
            BuildGridArray();
        }

        [JsonIgnore]
        public Bitmap Bitmap
        {
            get
            {
                if (bitmap == null) GenerateBitmap();
                return bitmap;
            }
        }

        [JsonIgnore]
        public string Filename { get; set; }
        public Point Dimensions { get; set; }
        public byte[][] GridArray { get; set; }
        public SpritePalette Palette { get; set; }
        public string Name { get; set; }

        public string BitmapData
        {
            get
            {
                if (bitmap == null) GenerateBitmap();
                using (MemoryStream ms = new MemoryStream())
                {
                    bitmap.Save(ms, ImageFormat.Png);
                    byte[] byteArray = ms.ToArray();
                    return Convert.ToBase64String(byteArray);
                }
            }
            set
            {
                byte[] bitmapData = Convert.FromBase64String(value);
                using (MemoryStream ms = new MemoryStream(bitmapData))
                {
                    bitmap = new Bitmap((Bitmap)Image.FromStream(ms));
                }
            }
        }

        public void Clear()
        {
            BuildGridArray();
        }

        public void SetGridSize(Point size)
        {
            Dimensions = size;

            // TODO - Copy from old grid array
            BuildGridArray();
        }

        private void BuildGridArray()
        {
            GridArray = new byte[Dimensions.Y][];
            for (int g = 0; g < Dimensions.Y; g++)
                GridArray[g] = new byte[Dimensions.X];
        }

        public Bitmap GenerateBitmap()
        {
            Canvas canvas = new();
            canvas.Height = Dimensions.Y;
            canvas.Width = Dimensions.X;
            Palette.RebuildBitmaps();
            for (int y = 0; y < Dimensions.Y; y++)
            {
                for (int x = 0; x < Dimensions.X; x++)
                {
                    byte val = GridArray[y][x];
                    var sprite = Palette.GetSprite(val);
                    if (sprite != null)
                    {
                        Point point = new Point(x, y);
                        var cellName = $"CELL{point.X}_{point.Y}";
                        var image = BitmapConverter.BitmapToImage(sprite.Bitmap);
                        image.Width = Dimensions.X;
                        image.Height = Dimensions.Y;
                        Canvas.SetLeft(image, point.X * Dimensions.X);
                        Canvas.SetTop(image, point.Y * Dimensions.Y);
                        image.Tag = point;
                        image.Name = cellName;
                        canvas.Children.Add(image);
                    }
                }
            }

            bitmap = Utility.BitmapConverter.CanvasToBitmap(canvas);
            return bitmap;
        }

        public static RoomInfo? Load(string fileName)
        {
            if (!File.Exists(fileName))
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Room File (*.8br)|*.8br";
                openFileDialog.FileName = Path.GetFileName(fileName);
                openFileDialog.Title = "Locate " + Path.GetFileName(fileName);
                if (lastPath != null) openFileDialog.InitialDirectory = lastPath;
                if (!openFileDialog.ShowDialog() ?? false) return null;
                lastPath = Path.GetDirectoryName(openFileDialog.FileName);

                fileName = openFileDialog.FileName;
            }

            string json = File.ReadAllText(fileName);
            var roomInfo = JsonSerializer.Deserialize<RoomInfo>(json);
            roomInfo.Filename = fileName;
            return roomInfo;
        }
    }
}