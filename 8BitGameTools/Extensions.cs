﻿namespace EightBitGameTools
{
    using System.Windows;
    using System.Windows.Media;

    public static class UIElementExtensions
    {
        public static T? FindControl<T>(this UIElement parent, string ControlName) where T : FrameworkElement
        {
            if (parent == null)
                return null;

            if (parent.GetType().IsAssignableTo(typeof(T)) && ((T)parent).Name == ControlName)
            {
                return (T)parent;
            }

            T? result = null;
            int count = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < count; i++)
            {
                UIElement child = (UIElement)VisualTreeHelper.GetChild(parent, i);

                var findControl = FindControl<T>(child, ControlName);
                if (findControl != null)
                {
                    result = findControl;
                    break;
                }
            }

            return result;
        }
    }
}
