﻿namespace EightBitGameTools
{
    using EightBitGameTools.Palette;
    using Microsoft.Win32;
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Text.Json;
    using System.Text.Json.Serialization;
    using System.Windows.Controls;
    using System.Windows.Media;
    using Image = System.Drawing.Image;

    public class SpriteInfo
    {
        private Bitmap bitmap = null;
        private static string lastPath = null;

        public SpriteInfo()
        {

        }

        public SpriteInfo(Point size, IndexedPalette palette)
        {
            Palette = palette;
            Name = "NewSprite";
            Dimensions = size;
            GridArray = new byte[size.Y][];
            for (int g = 0; g < size.Y; g++)
                GridArray[g] = new byte[size.X];
        }

        [JsonIgnore]
        public Bitmap Bitmap
        {
            get
            {
                if (bitmap == null) UpdateBitmap();
                return bitmap;
            }
        }

        [JsonIgnore]
        public string Filename { get; set; }
        public Point Dimensions { get; set; }
        public int GridSize { get; set; }
        public byte[][] GridArray { get; set; }
        public IndexedPalette Palette { get; set; }
        public string Name { get; set; }

        public string BitmapData
        {
            get
            {
                if (bitmap == null) UpdateBitmap();
                using (MemoryStream ms = new MemoryStream())
                {
                    bitmap.Save(ms, ImageFormat.Png);
                    byte[] byteArray = ms.ToArray();
                    return Convert.ToBase64String(byteArray);
                }
            }
            set
            {
                byte[] bitmapData = Convert.FromBase64String(value);
                using (MemoryStream ms = new MemoryStream(bitmapData))
                {
                    bitmap = new Bitmap((Bitmap)Image.FromStream(ms));
                }
            }
        }

        public void Clear()
        {
            for (var y = 0; y < Dimensions.Y; y++)
                for (var x = 0; x < Dimensions.X; x++)
                    GridArray[x][y] = 0;
        }

        public void SetGridSize(Point size)
        {

            // Build new grid
            var NewGridArray = new byte[size.Y][];
            for (int y = 0; y < size.Y; y++)
                NewGridArray[y] = new byte[size.X];

            // Copy from old grid array
            for (var y = 0; y < Dimensions.Y && y < size.Y; y++)
                for (var x = 0; x < Dimensions.X && x < size.X; x++)
                    NewGridArray[y][x] = GridArray[y][x];

            // Set new grid
            GridArray = NewGridArray;
            Dimensions = size;
        }

        public static SpriteInfo Load(string fileName)
        {
            if (!File.Exists(fileName))
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Sprite File (*.8bs)|*.8bs";
                openFileDialog.FileName = Path.GetFileName(fileName);
                openFileDialog.Title = "Locate " + Path.GetFileName(fileName);
                if (lastPath != null) openFileDialog.InitialDirectory = lastPath;
                if (!openFileDialog.ShowDialog() ?? false) return null;
                lastPath = Path.GetDirectoryName(openFileDialog.FileName);

                fileName = openFileDialog.FileName;
            }

            string json = File.ReadAllText(fileName);
            var spriteInfo = JsonSerializer.Deserialize<SpriteInfo>(json);
            spriteInfo.Filename = fileName;

            // Convert size format
            if (spriteInfo.GridSize > 0)
            {
                spriteInfo.Dimensions = new Point(spriteInfo.GridSize, spriteInfo.GridSize);
                spriteInfo.GridSize = 0;
                spriteInfo.Save();
            }

            spriteInfo.Filename = fileName;
            return spriteInfo;
        }

        public bool Save()
        {
            if(string.IsNullOrEmpty(Filename)) return false;
            return Save(Filename);
        }

        public bool Save(string filename)
        {
            if (string.IsNullOrEmpty(filename)) return false;
            if (string.IsNullOrEmpty(Name)) if (Name == null) Name = Path.GetFileNameWithoutExtension(filename);
            Filename = filename;
            var json = JsonSerializer.Serialize(this);
            File.WriteAllText(filename, json);
            return true;
        }

        public Bitmap UpdateBitmap()
        {
            Canvas canvas = new();
            canvas.Height = Dimensions.Y;
            canvas.Width = Dimensions.X;

            for (int y = 0; y < Dimensions.Y; y++)
            {
                for (int x = 0; x < Dimensions.X; x++)
                {
                    byte val = GridArray[y][x];
                    Point point = new Point(x, y);
                    if (val == 0) continue;

                    var colour = Palette.GetColor(val);
                    var rect = new System.Windows.Shapes.Rectangle
                    {
                        Tag = point,
                        Name = $"CELL{point.X}_{point.Y}",
                        Stroke = new SolidColorBrush(colour),
                        Fill = new SolidColorBrush(colour),
                        Width = 1,
                        Height = 1
                    };

                    Canvas.SetLeft(rect, point.X);
                    Canvas.SetTop(rect, point.Y);
                    canvas.Children.Add(rect);
                }
            }

            bitmap = Utility.BitmapConverter.CanvasToBitmap(canvas);
            return bitmap;
        }

        public void FlipVertical()
        {
            for (int y = 0; y < Dimensions.Y / 2; y++)
            {
                for (int x = 0; x < Dimensions.X; x++)
                {
                    var tmp = GridArray[y][x];
                    GridArray[y][x] = GridArray[Dimensions.Y - y - 1][x];
                    GridArray[Dimensions.Y - y - 1][x] = tmp;
                }
            }
        }

        public void FlipHorizontal()
        {
            for (int y = 0; y < Dimensions.Y; y++)
            {
                for (int x = 0; x < Dimensions.X / 2; x++)
                {
                    var tmp = GridArray[y][x];
                    GridArray[y][x] = GridArray[y][Dimensions.X - x - 1];
                    GridArray[y][Dimensions.X - x - 1] = tmp;
                }
            }

        }

        public void ShiftLeft()
        {
            for (int y = 0; y < Dimensions.Y; y++)
            {
                for (int x = 0; x < Dimensions.X - 1; x++)
                {
                    GridArray[y][x] = GridArray[y][x + 1];
                }

                GridArray[y][Dimensions.X - 1] = 0;
            }
        }

        public void ShiftRight()
        {
            for (int y = 0; y < Dimensions.Y; y++)
            {
                for (int x = Dimensions.X - 1; x > 0; x--)
                {
                    GridArray[y][x] = GridArray[y][x - 1];
                }

                GridArray[y][0] = 0;
            }
        }

        public void ShiftUp()
        {
            for (int y = 0; y < Dimensions.Y - 1; y++)
            {
                GridArray[y] = GridArray[y + 1];
            }

            GridArray[Dimensions.Y - 1] = new byte[Dimensions.X];
        }

        public void ShiftDown()
        {
            for (int y = Dimensions.Y - 1; y > 0; y--)
            {
                GridArray[y] = GridArray[y - 1];
            }

            GridArray[0] = new byte[Dimensions.X];
        }
    }
}